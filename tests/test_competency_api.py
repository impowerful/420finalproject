import flask_unittest
from CompetencyCloudApp import create_app

class TestForAPI(flask_unittest.ClientTestCase):
    app = create_app()
    
    def test_get_competencies(self, client):
        resp = client.get('/api/v1/competencies?page=1')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertIsNotNone(json['results'])
        self.assertIsNotNone(json['next_page'])
        self.assertIsNone(json['prev_page'])

    def test_get_competency(self, client):
        resp = client.get('/api/v1/competencies?id=00Q1')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertEqual(json['competency_id'], '00Q1')

    def test_post_competency(self, client):
        competency = {
            "competency_id":'00Q1',
            "competency_title":'test',
            "competency_achievement":'test',
            "competency_type":'test'}
        resp = client.post('/api/v1/competencies', json=competency)
        self.assertEqual(resp.status_code, 201)

    def test_put_competency(self, client):
        competency = {
            "competency_id":'00Q1',
            "competency_title":'test',
            "competency_achievement":'test',
            "competency_type":'test'}
        resp = client.post('/api/v1/competencies', json=competency)
        self.assertEqual(resp.status_code, 201)

    def test_delete_competency(self, client):
        resp = client.delete('/api/v1/competencies?id=00Q1')
        self.assertEqual(resp.status_code, 201)