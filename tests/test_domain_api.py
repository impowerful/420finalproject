import flask_unittest
from CompetencyCloudApp import create_app

class TestForAPI(flask_unittest.ClientTestCase):
    app = create_app()

    def test_get_domains(self, client):
        resp = client.get('/api/v1/domains?page=1')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertIsNotNone(json['results'])
        self.assertIsNotNone(json['next_page'])
        self.assertIsNone(json['prev_page'])

    def test_get_domain(self, client):
        resp = client.get('/api/v1/domains?id=1')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertEqual(json['domain_id'], '1')

    def test_post_domain(self, client):
        domain = {
                "domain_id": 1,
                "domain":'test',
                "domain_description":'test'
            }
        resp = client.post('/api/v1/domains', json=domain)
        self.assertEqual(resp.status_code, 201)

    def test_put_domain(self, client):
        domain = {
                "domain_id": 1,
                "domain":'test',
                "domain_description":'test'
            }
        resp = client.put('/api/v1/domains', json=domain)
        self.assertEqual(resp.status_code, 201)

    def test_delete_domain(self, client):
        resp = client.delete('/api/v1/domains?id=1')
        self.assertEqual(resp.status_code, 201)