import flask_unittest
from CompetencyCloudApp import create_app

class TestForAPI(flask_unittest.ClientTestCase):
    app = create_app()

    def test_get_elements(self, client):
        resp = client.get('/api/v1/elements?page=1')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertIsNotNone(json['results'])
        self.assertIsNotNone(json['next_page'])
        self.assertIsNone(json['prev_page'])

    def test_get_element(self, client):
        resp = client.get('/api/v1/elements?id=1')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertEqual(json['element_id'], '1')

    def test_post_element(self, client):
        element = {
                'element_id':1,
                'element_order':1,
                'element_name':'test',
                'element_criteria':'test',
                'competency':{
            "competency_id":'00Q1',
            "competency_title":'test',
            "competency_achievement":'test',
            "competency_type":'test'}
                }
        resp = client.post('/api/v1/elements', json=element)
        self.assertEqual(resp.status_code, 201)

    def test_put_element(self, client):
        element = {
                'element_id':1,
                'element_order':1,
                'element_name':'test',
                'element_criteria':'test',
                'competency':{
            "competency_id":'00Q1',
            "competency_title":'test',
            "competency_achievement":'test',
            "competency_type":'test'}
                }
        resp = client.put('/api/v1/elements', json=element)
        self.assertEqual(resp.status_code, 201)

    def test_delete_element(self, client):
        resp = client.delete('/api/v1/elements?id=1')
        self.assertEqual(resp.status_code, 201)