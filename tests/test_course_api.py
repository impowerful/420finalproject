import flask_unittest
from CompetencyCloudApp import create_app

class TestForAPI(flask_unittest.ClientTestCase):
    app = create_app()

    def test_get_courses(self, client):
        resp = client.get('/api/v1/courses?page=1')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertIsNotNone(json['results'])
        self.assertIsNotNone(json['next_page'])
        self.assertIsNone(json['prev_page'])

    def test_get_course(self, client):
        resp = client.get('/api/v1/courses?id=420-440-DW')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertEqual(json['course_id'], '420-440-DW')

    def test_post_course(self, client):
        course = {
            "course_id":'420-662-DW',
            "course_title":'test',
            "theory_hours":3,
            "lab_hours":3,
            "work_hours":3,
            "description":'test',
            "domain":{
                "domain_id": 1,
                "domain":'test',
                "domain_description":'test'
            },
            "term": {
                "term_id": 1,
                "term_name":'test'
            }}
        resp = client.post('/api/v1/courses', json=course)
        self.assertEqual(resp.status_code, 201)

    def test_put_course(self, client):
        course = {
            "course_id":'420-440-DW',
            "course_title":'test change title',
            "theory_hours":3,
            "lab_hours":3,
            "work_hours":3,
            "description":'test',
            "domain":{
                "domain_id": 1,
                "domain":'test',
                "domain_description":'test'
            },
            "term": {
                "term_id": 1,
                "term_name":'test'
            }}
        resp = client.put('/api/v1/courses', json=course)
        self.assertEqual(resp.status_code, 201)

    def test_delete_course(self, client):
        resp = client.delete('/api/v1/courses?id=420-661-DW')
        self.assertEqual(resp.status_code, 201)