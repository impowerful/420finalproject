import flask_unittest
from CompetencyCloudApp import create_app

class TestForAPI(flask_unittest.ClientTestCase):
    app = create_app()

    def test_get_terms(self, client):
        resp = client.get('/api/v1/terms?page=1')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertIsNotNone(json['results'])
        self.assertIsNotNone(json['next_page'])
        self.assertIsNone(json['prev_page'])

    def test_get_term(self, client):
        resp = client.get('/api/v1/terms?id=1')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertEqual(json['term_id'], '1')

    def test_post_term(self, client):
        term = {
                "term_id": 1,
                "term_name":'test'
            }
        resp = client.post('/api/v1/terms', json=term)
        self.assertEqual(resp.status_code, 201)

    def test_put_term(self, client):
        term = {
                "term_id": 1,
                "term_name":'test'
            }
        resp = client.put('/api/v1/terms', json=term)
        self.assertEqual(resp.status_code, 201)

    def test_delete_term(self, client):
        resp = client.delete('/api/v1/terms?id=1')
        self.assertEqual(resp.status_code, 201)