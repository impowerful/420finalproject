420 Final Project  
Competency Cloud App  
  
Group Members  
Marko Litovchenko 2133905, Yu Hua Yang 2133677, Oni Picotte 2036356  
  
Development Setup  
  
Install Python 3.7 or greater  
Create and activate virtual environment (e.g., python -m venv .venv)  
Install requirements using pip install -r requirements.txt  
Set DBUSER and set DBPWD for the PDBORA19C database  
Setup database by running the following command (you need to be in 420finalproject directory):  
flask --app CompetencyCloudApp init-db  
Or to remove the database:  
flask --app CompetencyCloudApp rm-db  
Run the flask app for debugging using flask --app CompetencyCloudApp --debug run  
  
  
Deployment Setup  
  
Install Python 3.7 or greater  
Create and activate virtual environment (e.g., python -m venv .venv)  
Install requirements using pip install -r requirements.txt  
Set DBUSER and set DBPWD for the PDBORA19C database  
Setup database by running the following command (you need to be in 420finalproject directory):  
flask --app CompetencyCloudApp init-db  
Or to remove the database:  
flask --app CompetencyCloudApp rm-db  
Allow the port through your firewall (e.g., ufw allow 8000)  
Create a config.py with the SECRET_KEY setting (on the same level as __init__.py in CompetencyCloudApp)  
Run the command gunicorn -w 2 -b 0.0.0.0:8000 'CompetencyCloudApp:create_app()'  

Gitlab link: https://gitlab.com/impowerful/420finalproject  
Website link: http://10.172.12.81:8000  
