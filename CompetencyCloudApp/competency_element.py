class CompetencyElement:
    def __init__(self,element_id,element_name,element_order,element_criteria,competency_id):
        
        if not isinstance(element_name,str):
            raise TypeError('element name is not a string')
        if not isinstance(element_order,int):
            raise TypeError('element order is not a int')
        if not isinstance(element_criteria,str):
            raise TypeError('element criteria is not a string')
        if not isinstance(competency_id,str):
            raise TypeError('competency id is not a string')
        
        if isinstance(element_id,str) or isinstance(element_id,int):
            self.element_id=element_id
        else:
            raise TypeError('element id is wrong')
        self.element_name=element_name
        self.element_order=element_order
        self.element_criteria=element_criteria
        self.competency_id=competency_id
    
    def __repr__(self):
        return f'Element({self.element_id}, {self.element_name}, {self.element_order}, {self.element_criteria}, {self.competency_id})'

    def from_json(competency_element_dict):
        pass    
    

from flask_wtf import FlaskForm
from wtforms import SelectField, IntegerField, StringField
from wtforms.validators import DataRequired  
    
class AddCompetencyElementForm(FlaskForm):
    element_name = StringField('Element Name: ',validators=[DataRequired()])
    element_order = IntegerField('Element Order: ', validators=[DataRequired()])
    element_criteria = StringField('Element Criteria: ',validators=[DataRequired()])

class EditCompetencyElementForm(FlaskForm):
    element_id = SelectField('Element ID: ', validators=[DataRequired()])
    element_name = StringField('Element Name: ',validators=[DataRequired()])
    element_order = IntegerField('Element Order: ', validators=[DataRequired()])
    element_criteria = StringField('Element Criteria: ',validators=[DataRequired()])

class DeleteCompetencyElementForm(FlaskForm):
    element_id = SelectField('Element that you want to delete: ', validators=[DataRequired()])