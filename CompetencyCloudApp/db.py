import oracledb
import os
from CompetencyCloudApp.course import Course
from CompetencyCloudApp.competency import Competency
from CompetencyCloudApp.domain import Domain
from CompetencyCloudApp.element import Element
from CompetencyCloudApp.term import Term
from CompetencyCloudApp.user import User
from CompetencyCloudApp.profile import Profile
from CompetencyCloudApp.competency_element import CompetencyElement
from CompetencyCloudApp.course_element import CourseElement

# Database
class Database:
  def __init__(self, autocommit=True):
    self.__connection = self.__connect()
    self.__connection.autocommit = autocommit

  # Execute a file to the db
  def run_file(self, file_path):
    statement_parts = []
    with self.__connection.cursor() as cursor:
      with open(file_path, 'r') as f:
        for line in f:
          statement_parts.append(line)
          if line.strip('\n').strip('\n\r').strip().endswith(';'):
            statement = "".join(
              statement_parts).strip().rstrip(';')
            if statement:
              try:
                cursor.execute(statement)
              except Exception as e:
                print(e)
            statement_parts = []

  def add_user(self, user, group_name='member'):
    if not isinstance(user, User):
      raise TypeError()
    with self.__connection.cursor() as cursor:
      cursor.execute('insert into users (email, password, name, group_name, blocked_status) values (:email, :password, :name, :group_name, :blocked_status)',
                     email=user.email,
                     password=user.password,
                     name=user.name,
                     group_name=group_name,
                     blocked_status=0)
        
  def get_user_by_email(self, email):
    if not isinstance(email, str):
      raise TypeError()
    with self.__connection.cursor() as cursor:
      results = cursor.execute('select user_id, email, password, name, group_name from users where email=:email', email=email)
      for row in results:
        user = User(email=row[1], password=row[2], name=row[3], id=row[0], group_name=row[4])
        user.id = row[0]
        return user
    return None
  
  def get_user_by_id(self, id):
    with self.__connection.cursor() as cursor:
      results = cursor.execute('select user_id, email, password, name, group_name from users where user_id=:id', id=id)
      for row in results:
        user = User(email=row[1], password=row[2], name=row[3], id=row[0], group_name=row[4])
        return user

    
  # Get Term
  def get_term(self,term_id):
    if not isinstance(term_id,int):
      raise TypeError('id need to be of type int')

    with self.__get_cursor() as cursor:
      result = cursor.execute(
        'select term_name from terms where term_id = :term_id',
        term_id=term_id
      )

      for row in result:
        term = Term(term_id=term_id,term_name=row[0])
      return term
    
  # Get Terms
  def get_terms(self,page_num=1,page_size=50):
    terms = []

    with self.__get_cursor() as cursor:
      offset = (page_num-1)*page_size
      prev_page = None
      next_page = None

      result_count = cursor.execute('select count(*) from terms')
      count = result_count.fetchone()[0]

      result = cursor.execute(
        'select term_id, term_name from terms order by term_id offset :offset rows fetch next :page_size rows only',
        offset=offset,
        page_size=page_size
      )

      for row in result:
        term = Term(term_id=row[0],term_name=row[1])
        terms.append(term)
      
    if page_num > 1:
      prev_page = page_num - 1
    if (count-(page_size*page_num)) > 0:
      next_page = page_num + 1

    return terms, prev_page, next_page
  
  # Add Term
  def add_term(self,term):
    if not isinstance(term,Term):
      raise TypeError('term is not of type Term')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'insert into terms (term_id,term_name) values (:term_id,:term_name)',
          term_id=term.term_id,
          term_name=term.term_name
        )
      except:
        raise ValueError('DB Error')
      
  # Update Term
  def update_term(self,term):
    if not isinstance(term,Term):
      raise TypeError('term is not of type Term')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'update terms set term_name=:term_name where term_id=:term_id',
          term_name=term.term_name,
          term_id=term.term_id
        )
      except:
        raise ValueError('DB Error')
      
  # Delete Term
  def delete_term(self,id):
    if not isinstance(id,int):
      raise TypeError('id is not of type int')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'delete from terms where term_id = :id',
          id=id
        )
      except:
        raise ValueError('DB Error')
  
  # Get Domain by id
  def get_domain_by_id(self,domain_id):
    if isinstance(domain_id,int) or isinstance(domain_id,str):
      with self.__get_cursor() as cursor:
        result = cursor.execute(
          'select domain,domain_description from domains where domain_id = :domain_id',
          domain_id=domain_id
        )
        for row in result:
          domain = Domain(domain_id=domain_id,domain=row[0],domain_description=row[1])
        return domain
    raise TypeError('id need to be of type int')

    
    
  # Get Domains
  def get_domains(self,page_num=1,page_size=50):
    domains = []

    with self.__get_cursor() as cursor:
      offset = (page_num-1)*page_size
      prev_page = None
      next_page = None

      result_count = cursor.execute('select count(*) from domains')
      count = result_count.fetchone()[0]

      results = cursor.execute(
        'select domain_id,domain,domain_description from domains order by domain_id offset :offset rows fetch next :page_size rows only',
        offset=offset,
        page_size=page_size
      )
      for row in results:
        domain = Domain(
          domain_id=row[0],
          domain=row[1],
          domain_description=row[2]
        )
        domains.append(domain)

    if page_num > 1:
      prev_page = page_num - 1
    if (count-(page_size*page_num)) > 0:
      next_page = page_num + 1

    return domains, prev_page, next_page
  
  # Add Domain
  def add_domain(self,domain):
    if not isinstance(domain,Domain):
      raise TypeError('domain is not of type Domain')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'insert into domains (domain,domain_description) values (:domain,:domain_description)',
          domain=domain.domain,
          domain_description=domain.domain_description
        )
      except:
        raise ValueError('DB Error')
      
  # Update Domain
  def update_domain(self,domain):
    if not isinstance(domain,Term):
      raise TypeError('domain is not of type Domain')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'update domains set domain_description=:domain_description where domain=:domain',
          domain_description=domain.domain_description,
          domain=domain.domain
        )
      except:
        raise ValueError('DB Error')
  
  # Delete domain
  def delete_domain(self,id):
    if not isinstance(id,int):
      raise TypeError('id is not of type int')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'delete from domains where domain_id = :id',
          id=id
        )
      except:
        raise ValueError('DB Error')
  
  # Get Course by id
  def get_course(self,id):
    with self.__get_cursor() as cursor:
      course = None
      results = cursor.execute(
        'select course_id,course_title,theory_hours,lab_hours,work_hours,description,domain_id,term_id from courses where course_id = :id',
        id=id
      )
      for row in results:
        domain = self.get_domain_by_id(row[6])
        term = self.get_term(row[7])
        course = Course(
          course_id=row[0],
          course_title=row[1],
          theory_hours=row[2],
          lab_hours=row[3],
          work_hours=row[4],
          description=row[5],
          domain=domain,
          term=term
        )
    return course
  
  # Get Competency by id
  def get_competency(self, id):
    with self.__get_cursor() as cursor:
      results = cursor.execute(
        'select competency_id,competency,competency_achievement,competency_type from competencies where competency_id=:id',
        id=id
      )
      for row in results:
        competency=Competency(
          competency_id=row[0],
          competency_title=row[1],
          competency_achievement=row[2],
          competency_type=row[3]
        )
      return competency
    
  # Get Course Elements
  def get_elements_course_id(self,course_id):
    elements = []
    with self.__get_cursor() as cursor:
      results = cursor.execute(
        'select element_id,element_hours \
        from courses_elements \
        where course_id=:course_id', 
        course_id=course_id
      )
      for row in results:
        element = CourseElement(
          course_id=course_id,
          element_id=row[0],
          element_hours=row[1],
        )
        elements.append(element)
      return elements
  
  # Get all Elemments
  def get_all_elements(self):
    elements = []
    with self.__get_cursor() as cursor:
      results = cursor.execute(
        'select element, element_id \
        from elements'
      )
      for row in results:
        elements.append((row[1], row[0]))
      return elements
  
  # Get Competency Elements
  def get_elements_competency_id(self,competency_id):
    elements = []
    with self.__get_cursor() as cursor:
      results = cursor.execute(
        'select element_id,element,element_order,element_criteria \
        from elements \
        where competency_id=:competency_id', 
        competency_id=competency_id
      )
      for row in results:
        element = CompetencyElement(
          element_id=row[0],
          element_name=row[1],
          element_order=row[2],
          element_criteria=row[3],
          competency_id=competency_id
        )
        elements.append(element)
      return elements
    
  # Get Courses
  def get_courses(self,page_num=1,page_size=50):
    courses = []
    with self.__get_cursor() as cursor:
      offset = (page_num-1)*page_size
      prev_page = None
      next_page = None
      result_count = cursor.execute('select count(*) from courses')
      count = result_count.fetchone()[0]
      results = cursor.execute(
        'select course_id,course_title,theory_hours,lab_hours,work_hours,description,domain_id,term_id from courses order by course_id offset :offset rows fetch next :page_size rows only',
        offset=offset,
        page_size=page_size
      )
      for row in results:
        domain = self.get_domain_by_id(row[6])
        term = self.get_term(row[7])
        course = Course(
          course_id=row[0],
          course_title=row[1],
          theory_hours=row[2],
          lab_hours=row[3],
          work_hours=row[4],
          description=row[5],
          domain=domain,
          term=term
        )
        courses.append(course)

    if page_num > 1:
      prev_page = page_num - 1
    if (count-(page_size*page_num)) > 0:
      next_page = page_num + 1
    
    return courses, prev_page, next_page
  
  # Add Course
  def add_course(self,course):
    if not isinstance(course,Course):
      raise TypeError('course is not of type Course')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'insert into courses (course_id,course_title,theory_hours,lab_hours,work_hours,description,domain_id,term_id) values (:course_id,:course_title,:theory_hours,:lab_hours,:work_hours,:description,:domain_id,:term_id)',
          course_id=course.course_id,
          course_title=course.course_title,
          theory_hours=course.theory_hours,
          lab_hours=course.lab_hours,
          work_hours=course.work_hours,
          description=course.description,
          domain_id=course.domain.domain_id,
          term_id=course.term.term_id
        )
      except:
        raise ValueError('DB Error')
    
  # Add Competency
  def add_competency(self, competency):
    if not isinstance(competency, Competency):
      raise TypeError('competency is not of type Competency')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'insert into competencies (competency_id,competency,competency_achievement,competency_type) values (:competency_id,:competency,:competency_achievement,:competency_type)',
          competency_id=competency.competency_id,
          competency=competency.competency_title,
          competency_achievement=competency.competency_achievement,
          competency_type=competency.competency_type
        )
      except Exception as e:
        raise ValueError(str(e))
      
  # Update Course
  def update_course(self,course):
    if not isinstance(course,Course):
      raise TypeError('course is not of type Course')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'update courses set course_title = :course_title,theory_hours = :theory_hours,lab_hours = :lab_hours, work_hours = :work_hours,description = :description, domain_id = :domain_id,term_id = :term_id where course_id = :course_id',
          course_title=course.course_title,
          theory_hours=course.theory_hours,
          lab_hours=course.lab_hours,
          work_hours=course.work_hours,
          description=course.description,
          domain_id=course.domain.domain_id,
          term_id=course.term.term_id,
          course_id=course.course_id
        )
      except:
        raise ValueError('DB Error')
      
  # Delete Course
  def delete_course(self,id):
    if not isinstance(id,str):
      raise TypeError('id is not of type str')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'delete from courses where course_id = :id',
          id=id
        )
      except:
        raise ValueError('DB Error')
  
  # Modify Domain
  def modify_domain(self,domain):
    if not isinstance(domain, Domain):
      raise TypeError('domain is not a type of Domain')
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'update domains set domain=:domain,domain_description=:domain_description where domain_id=:domain_id',
          domain=domain.domain,
          domain_description=domain.domain_description,
          domain_id=domain.domain_id
        )
      except:
        raise ValueError('DB Error')
      
  def delete_domain(self,id):
    if not isinstance(id, str):
      raise TypeError('id is not of type str')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'delete from courses where domain_id = :id',
          id=id
        )
        cursor.execute(
          'delete from domains where domain_id = :id',
          id=id
        )
      except:
        raise ValueError('DB Error')
        
  # Modify Competency
  def modify_competency(self, competency):
    if not isinstance(competency,Competency):
      raise TypeError('competency is not a type of Competency')
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'update competencies set competency=:competency,competency_achievement=:competency_achievement,competency_type=:competency_type where competency_id=:competency_id',
          competency=competency.competency_title,
          competency_achievement=competency.competency_achievement,
          competency_type=competency.competency_type,
          competency_id=competency.competency_id
        )
      except:
        raise ValueError('DB Error')
      
  # Delete Competency
  def delete_competency(self,id):
    if not isinstance(id,str):
      raise TypeError('id is not of type str')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'delete from elements where competency_id = :id',
          id=id
        )
        cursor.execute(
          'delete from competencies where competency_id = :id',
          id=id
        )
      except:
        raise ValueError('DB Error')
      
  # Add Competency
  def add_competency(self,competency):
    if not isinstance(competency,Competency):
      raise TypeError('competency is not of type Competency')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'insert into competencies (competency_id,competency,competency_achievement,competency_type) values (:competency_id,:competency_title,:competency_achievement,:competency_type)',
          competency_id=competency.competency_id,
          competency_title=competency.competency_title,
          competency_achievement=competency.competency_achievement,
          competency_type=competency.competency_type
        )
      except:
        raise ValueError('DB Error')
  
  # Update Competency
  def update_competency(self,competency):
    if not isinstance(competency,Competency):
      raise TypeError('competency is not of type Competency')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'update competencies set competency_title=:competency_title,competency_achievement=:competency_achievement,competency_type=:competency_type where competency_id=:competency_id',
          competency_title=competency.competency_title,
          competency_achievement=competency.competency_achievement,
          competency_type=competency.competency_type,
          competency_id=competency.competency_id
        )
      except:
        raise ValueError('DB Error')

  # Get Competencies
  def get_competencies(self,page_num=1,page_size=50):
    competencies = []
    
    with self.__get_cursor() as cursor:
      offset = (page_num-1)*page_size
      prev_page = None
      next_page = None

      result_count = cursor.execute('select count(*) from competencies')
      count = result_count.fetchone()[0]

      results = cursor.execute(
        'select competency_id,competency,competency_achievement,competency_type from competencies order by competency_id offset :offset rows fetch next :page_size rows only',
        offset=offset,
        page_size=page_size
      )
      for row in results:
        competency = Competency(
          competency_id=row[0],
          competency_title=row[1],
          competency_achievement=row[2],
          competency_type=row[3]
        )
        competencies.append(competency)

    if page_num > 1:
      prev_page = page_num - 1
    if (count-(page_size*page_num)) > 0:
      next_page = page_num + 1

    return competencies, prev_page, next_page
  
  # Get Competency
  def get_competency(self,id):
    with self.__get_cursor() as cursor:
      results = cursor.execute(
        'select competency_id,competency,competency_achievement,competency_type from competencies where competency_id = :id',
        id=id
      )
      for row in results:
        competency = Competency(
          competency_id=row[0],
          competency_title=row[1],
          competency_achievement=row[2],
          competency_type=row[3]
        )
    return competency
  
  # Delete competency
  def delete_competency(self,id):
    if not isinstance(id,str):
      raise TypeError('id is not of type str')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'delete from competencies where competency_id = :id',
          id=id
        )
      except:
        raise ValueError('DB Error')
  
  # Get Element by id
  def get_element(self,id):
    with self.__get_cursor() as cursor:
      element = None
      results = cursor.execute(
        'select element_id,element_order,element,element_criteria,competency_id from elements where element_id = :id',
        id=id
      )
      for row in results:
        competency = self.get_competency(row[4])
        element = Element(
          element_id=row[0],
          element_order=row[1],
          element_name=row[2],
          element_criteria=row[3],
          competency=competency
        )
    return element

  # Get Elements
  def get_elements(self,page_num=1,page_size=50):
    elements = []

    with self.__get_cursor() as cursor:
      offset = (page_num-1)*page_size
      prev_page = None
      next_page = None
      result_count = cursor.execute('select count(*) from elements')
      count = result_count.fetchone()[0]
      results = cursor.execute(
        'select element_id,element_order,element,element_criteria,competency_id from elements order by element_id offset :offset rows fetch next :page_size rows only',
        offset=offset,
        page_size=page_size
      )
      for row in results:
        competency = self.get_competency(row[4])
        element = Element(
          element_id=row[0],
          element_order=row[1],
          element_name=row[2],
          element_criteria=row[3],
          competency=competency
        )
        elements.append(element)

    if page_num > 1:
      prev_page = page_num - 1
    if (count-(page_size*page_num)) > 0:
      next_page = page_num + 1
    
    return elements, prev_page, next_page
  
  # Update Element
  def update_element(self,element):
    if not isinstance(element,Element):
      raise TypeError('element is not of type Element')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'update elements set element_order=:element_order,element_name=:element_name,element_criteria=:element_criteria,competency_id=:competency_id where element_id=:element_id',
          element_order=element.element_order,
          element_name=element.element_name,
          element_criteria=element.element_criteria,
          competency_id=element.competency.competency_id,
          element_id=element.element_id
        )
      except:
        raise ValueError('DB Error')
      
  # Delete element
  def delete_element(self,id):
    if not isinstance(id,int):
      raise TypeError('id is not of type int')
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'delete from elements where element_id = :id',
          id=id
        )
      except:
        raise ValueError('DB Error')

  # Get Profile
  def get_profile_by_email(self, email):
    profiles = []
    if not isinstance(email, str):
      raise TypeError('email is not a str')
    with self.__get_cursor() as cursor:
      results = cursor.execute(
        'select name, email from users where email=:email', email=email)
      for row in results:
        profile = Profile(
            profile_name=row[0],
            profile_email=row[1]
        )
        profiles.append(profile)
    return profiles
  
  # Update User Password
  def update_user_password(self, email, hash):
    if not isinstance(email, str):
      raise TypeError('email is not a str')
    if not isinstance(hash, str):
      raise TypeError('password is not a str')
    with self.__get_cursor() as cursor:
      cursor.execute('update users set password=:hash where email=:email', hash=hash, email=email)
  
  # Get all users in a group
  def get_all_user_profiles(self, group_name):
    users = []
    with self.__get_cursor() as cursor:
      results = cursor.execute(
        "select name, email from users where group_name=:group_name", group_name=group_name)
      for row in results:
        profile = Profile(
            profile_name=row[0],
            profile_email=row[1]
      )
        users.append(profile)
    return users

  # Move member to desired group
  def move_user_to_group(self, email, group_name):
    if not isinstance(email, str):
      raise TypeError('email is not a str')
    if not isinstance(group_name, str):
      raise TypeError('group is not a str')
    with self.__get_cursor() as cursor:
      cursor.execute('update users set group_name=:group_name where email=:email', group_name=group_name, email=email)
  
  # Deletes a user from the database
  def delete_user(self, email):
    if not isinstance(email, str):
      raise TypeError('email is not a str')
    with self.__get_cursor() as cursor:
      cursor.execute('delete from users where email=:email', email=email)
  
  # Blocks a user
  def block_user(self, email):
    if not isinstance(email, str):
      raise TypeError('email is not a str')
    with self.__get_cursor() as cursor:
      cursor.execute('update users set blocked_status=1 where email=:email', email=email)
      
  # Unblocks a user
  def unblock_user(self, email):
    if not isinstance(email, str):
      raise TypeError('email is not a str')
    with self.__get_cursor() as cursor:
      cursor.execute('update users set blocked_status=0 where email=:email', email=email)
      
  # Get all blocked user
  def get_all_blocked_user(self):
    blocked_members=[]
    with self.__get_cursor() as cursor:
      results = cursor.execute('select name, email from users where blocked_status=1')
      for row in results:
        profile = Profile(
            profile_name=row[0],
            profile_email=row[1]
      )
        blocked_members.append(profile)
    return blocked_members
      
  # Get user blocked status
  def get_blocked_status_by_email(self, email):
    if not isinstance(email, str):
      raise TypeError('email is not a str')
    with self.__get_cursor() as cursor:
      results = cursor.execute('select blocked_status from users where email=:email', email=email)
      for row in results:
        status = row[0]
      return status
    
  # Add new course element
  def add_course_elements(self, course_id, element_id, element_hours):
    if not isinstance(course_id, str):
      raise TypeError('competency id is not a str')
    if not isinstance(element_id,str):
      raise TypeError('element id is not a str')
    if isinstance(element_hours,int) or isinstance(element_hours,float):
      with self.__get_cursor() as cursor:
        try:
          cursor.execute('insert into courses_elements (course_id, element_id, element_hours) values (:course_id, :element_id, :element_hours)',
                          course_id=course_id,
                          element_id=element_id,
                          element_hours=element_hours)     
        except:
          raise ValueError('DB Error')
    else:
      raise TypeError('element hour is not a number')
    
  # Add new competency element
  def add_competency_elements(self, element):
    if not isinstance(element, CompetencyElement):
      raise TypeError('element is not a Element')
    with self.__get_cursor() as cursor:
      try:
        cursor.execute('insert into elements (element_order, element, element_criteria, competency_id) values (:element_order, :element, :element_criteria, :competency_id)',
                        element_order=element.element_order,
                        element=element.element_name,
                        element_criteria=element.element_criteria,
                        competency_id=element.competency_id)
      except:
          raise ValueError('DB Error')
    
  # Edit course element
  def edit_course_elements(self, course_id, element_id, element_hours):      
    if not isinstance(course_id, str):
      raise TypeError('competency id is not a str')
    if not isinstance(element_id,str):
      raise TypeError('element id is not a str')
    if isinstance(element_hours,int) or isinstance(element_hours,float):
      with self.__get_cursor() as cursor:
        try:
          cursor.execute('update courses_elements set element_hours=:element_hours where course_id=:course_id and element_id=:element_id',
                          element_hours=element_hours,
                          course_id=course_id,
                          element_id=element_id
                          )     
        except:
          raise ValueError('DB Error')
    else:
      raise TypeError('element hour is not a number') 
                                                             
  # Edit competency element
  def edit_competency_elements(self, element):      
    if not isinstance(element, CompetencyElement):
      raise TypeError('element is not of type Element')        
    
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'update elements set element_order=:element_order,element=:element_name,element_criteria=:element_criteria,competency_id=:competency_id where element_id=:element_id',
          element_order=element.element_order,
          element_name=element.element_name,
          element_criteria=element.element_criteria,
          competency_id=element.competency_id,
          element_id=element.element_id
        )
      except:
        raise ValueError('DB Error')          
       
  # Delete course element
  def delete_course_elements(self, course_id, element_id):
    if not isinstance(course_id, str):
      raise TypeError('competency id is not a str')
    if not isinstance(element_id,str):
      raise TypeError('element id is not a str')
    with self.__get_cursor() as cursor:
      try:
        cursor.execute(
          'delete from courses_elements where course_id=:course_id and element_id=:element_id',
          course_id=course_id,
          element_id=element_id
        )
      except:
        raise ValueError('DB Error')
  
  # Delete competency element
  def delete_competency_elements(self, competency_id, element_id):
    if not isinstance(competency_id, str):
      raise TypeError('competency id is not a str')
    if not isinstance(element_id,str):
      raise TypeError('element id is not a str')
    with self.__get_cursor() as cursor:
        try:
          cursor.execute(
            'delete from elements where competency_id =:competency_id and element_id=:element_id',
            competency_id=competency_id,
            element_id=element_id
          )
        except:
          raise ValueError('DB Error')
        
  # Get course element with element id
  def get_course_elements_by_element_id(self,element_id):
    if not isinstance(element_id,str):
      raise TypeError('element id is not a str')
    elements = []
    with self.__get_cursor() as cursor:
      results = cursor.execute(
        'select course_id,element_id,element_hours\
        from courses_elements \
        where element_id=:element_id', 
        element_id=element_id
      )
      for row in results:
        element = CourseElement(
          course_id=row[0],
          element_id=row[1],
          element_hours=row[2]
        )
        elements.append(element)
      return elements
    
  # Get competency element with element id
  def get_competency_elements_by_element_id(self,element_id):
    if not isinstance(element_id,str):
      raise TypeError('element id is not a str')
    elements = []
    with self.__get_cursor() as cursor:
      results = cursor.execute(
        'select element_id,element_order,element,element_criteria,competency_id\
        from elements \
        where element_id=:element_id', 
        element_id=element_id
      )
      for row in results:
        element = CompetencyElement(
          element_id=row[0],
          element_order=row[1],
          element_name=row[2],
          element_criteria=row[3],
          competency_id=row[4]
        )
        elements.append(element)
      return elements
        
  # ------------------------------------------------------------------------
  # SEARCH METHODS
  # ------------------------------------------------------------------------
  
  def search_whole_db(self, searched_query):
    searched_course_ids = self.search_course_id(searched_query)
    searched_course_titles = self.search_course_title(searched_query)
    searched_course_descriptions = self.search_course_description(searched_query)
    searched_competency_names = self.search_competency_name(searched_query)
    searched_competency_achievements = self.search_competency_achievement(searched_query)
    searched_element_names = self.search_element_name(searched_query)
    searched_element_criterias = self.search_element_criteria(searched_query)
    searched_domain_names = self.search_domain_name(searched_query)
    searched_domain_descriptions = self.search_domain_description(searched_query)
    
    all_course_results = searched_course_ids + searched_course_titles + searched_course_descriptions
    all_competency_results = searched_competency_names + searched_competency_achievements
    all_element_results = searched_element_names + searched_element_criterias
    all_domain_results = searched_domain_names + searched_domain_descriptions
    
    return all_course_results, all_competency_results, all_element_results, all_domain_results
  
  def search_course_id(self, searched_query):
    with self.__get_cursor() as cursor:
      try:
        matches = []
        searched_query = f'\'%{searched_query.lower()}%\''
        sql_statement = """select course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id from courses where lower(course_id) like """ + searched_query
        
        results = cursor.execute(sql_statement)
        
        for row in results:
          domain = self.get_domain_by_id(row[6])
          term = self.get_term(row[7])
          course = Course(row[0], row[1], row[2], row[3], row[4], row[5], domain, term)
          matches.append(course)
        return matches

      except:
          raise ValueError('DB Error') 
      
  def search_course_title(self, searched_query):
    with self.__get_cursor() as cursor:
      try:
        matches = []
        searched_query = f'\'%{searched_query.lower()}%\''
        sql_statement = """select course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id from courses where lower(course_title) like """ + searched_query
        
        results = cursor.execute(sql_statement)
        
        for row in results:
          domain = self.get_domain_by_id(row[6])
          term = self.get_term(row[7])
          course = Course(row[0], row[1], row[2], row[3], row[4], row[5], domain, term)
          matches.append(course)
        return matches

      except:
          raise ValueError('DB Error')
  
  def search_course_description(self, searched_query):
    with self.__get_cursor() as cursor:
      try:
        matches = []
        searched_query = f'\'%{searched_query.lower()}%\''
        sql_statement = """select course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id from courses where lower(description) like """ + searched_query
        
        results = cursor.execute(sql_statement)
        
        for row in results:
          domain = self.get_domain_by_id(row[6])
          term = self.get_term(row[7])
          course = Course(row[0], row[1], row[2], row[3], row[4], row[5], domain, term)
          matches.append(course)
        return matches

      except:
          raise ValueError('DB Error')
  
  def search_competency_name(self, searched_query):
    with self.__get_cursor() as cursor:
      try:
        matches = []
        searched_query = f'\'%{searched_query.lower()}%\''
        sql_statement = """select competency_id, competency, competency_achievement, competency_type from competencies where lower(competency) like """ + searched_query
        
        results = cursor.execute(sql_statement)
        
        for row in results:
          competency = Competency(row[0], row[1], row[2], row[3])
          matches.append(competency)
        return matches

      except:
          raise ValueError('DB Error')
  
  def search_competency_achievement(self, searched_query):
    with self.__get_cursor() as cursor:
      try:
        matches = []
        searched_query = f'\'%{searched_query.lower()}%\''
        sql_statement = """select competency_id, competency, competency_achievement, competency_type from competencies where lower(competency_achievement) like """ + searched_query
        
        results = cursor.execute(sql_statement)
        
        for row in results:
          competency = Competency(row[0], row[1], row[2], row[3])
          matches.append(competency)
        return matches

      except:
          raise ValueError('DB Error')
  
  def search_element_name(self, searched_query):
    with self.__get_cursor() as cursor:
      try:
        matches = []
        searched_query = f'\'%{searched_query.lower()}%\''
        sql_statement = """select element_id, element, element_order, element_criteria, competency_id from elements where lower(element) like """ + searched_query
        
        results = cursor.execute(sql_statement)
        
        for row in results:
          element = CompetencyElement(row[0], row[1], row[2], row[3], row[4])
          matches.append(element)
        return matches

      except:
          raise ValueError('DB Error')
  
  def search_element_criteria(self, searched_query):
    with self.__get_cursor() as cursor:
      try:
        matches = []
        searched_query = f'\'%{searched_query.lower()}%\''
        sql_statement = """select element_id, element, element_order, element_criteria, competency_id from elements where lower(element_criteria) like """ + searched_query
        
        results = cursor.execute(sql_statement)
        
        for row in results:
          element = CompetencyElement(row[0], row[1], row[2], row[3], row[4])
          matches.append(element)
        return matches

      except:
          raise ValueError('DB Error')
        
  def search_domain_name(self, searched_query):
    with self.__get_cursor() as cursor:
      try:
        matches = []
        searched_query = f'\'%{searched_query.lower()}%\''
        sql_statement = """select domain_id, domain, domain_description from domains where lower(domain) like """ + searched_query
        
        results = cursor.execute(sql_statement)
        
        for row in results:
          domain = Domain(row[0], row[1], row[2])
          matches.append(domain)
        return matches

      except:
          raise ValueError('DB Error')
  
  def search_domain_description(self, searched_query):
    with self.__get_cursor() as cursor:
      try:
        matches = []
        searched_query = f'\'%{searched_query.lower()}%\''
        sql_statement = """select domain_id, domain, domain_description from domains where lower(domain_description) like """ + searched_query
        
        results = cursor.execute(sql_statement)
        
        for row in results:
          domain = Domain(row[0], row[1], row[2])
          matches.append(domain)
        return matches

      except:
          raise ValueError('DB Error')
  
  # Close connection if connection is open
  def close(self):
    '''Closes the connection'''
    if self.__connection is not None:
      self.__connection.close()
      self.__connection = None

  # Get cursor from connection
  def __get_cursor(self):
    for i in range(3):
      try:
        return self.__connection.cursor()
      except Exception as e:
        # Might need to reconnect
        self.__reconnect()

  # Reconnect to db
  def __reconnect(self):
    try:
      self.close()
    except oracledb.Error as f:
      pass
    self.__connection = self.__connect()

  # Connect to the db
  def __connect(self):
    return oracledb.connect(
      user=os.environ['DBUSER'], 
      password=os.environ['DBPWD'],
      host="198.168.52.211", 
      port=1521, 
      service_name="pdbora19c.dawsoncollege.qc.ca")

if __name__ == '__main__':
  print('Provide file to initialize database')
  file_path = input()
  if os.path.exists(file_path):
    db = Database()
    db.run_file(file_path)
    db.close()
  else:
    print('Invalid Path')
