import os
from flask import Flask, render_template
from flask_login import LoginManager
import secrets
import click
from .dbmanager import get_db, init_db, remove_db, close_db
from .course_view import bp as course_bp
from .auth_view import bp as auth_bp
from .domain_view import bp as domain_bp
from .competency_views import bp as competency_bp
from .api.api import bp as api_bp
from .home_views import bp as home_bp
from .profile_views import bp as profile_bp
from .dashboard_view import bp as dashboard_bp
from .competency_element_view import bp as competency_element_bp
from .course_element_view import bp as course_element_bp

def create_app(test_config=None):
    app = Flask(__name__)

    app.config.from_mapping(
        SECRET_KEY=secrets.token_urlsafe(32),
        IMAGE_PATH=os.path.join(app.instance_path, 'avatars')
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    app.register_blueprint(home_bp)
    
    app.register_blueprint(course_bp)
    
    app.register_blueprint(competency_bp)
    
    app.register_blueprint(auth_bp)
    
    app.register_blueprint(profile_bp)

    app.register_blueprint(domain_bp)
    
    app.register_blueprint(api_bp)
    
    app.register_blueprint(dashboard_bp)
    
    app.register_blueprint(competency_element_bp)
    
    app.register_blueprint(course_element_bp)

    app.teardown_appcontext(close_db)

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('page_not_found.html'), 404

    app.cli.add_command(init_db_command)
    app.cli.add_command(remove_db_command)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return get_db().get_user_by_id(int(user_id))

    os.makedirs(app.config['IMAGE_PATH'], exist_ok=True)
    
    return app

@click.command('init-db')
def init_db_command():
    init_db()
    click.echo('Initialized database')
    
@click.command('rm-db')
def remove_db_command():
    remove_db()
    click.echo('Removed database')
