from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required

from CompetencyCloudApp.domain import Domain,DomainForm

from .dbmanager import get_db

bp = Blueprint('domains',__name__,url_prefix='/domains/')

@bp.route('/',methods=['GET','POST'])
def get_domains():
  try:
    domains,prev_page,next_page = get_db().get_domains()
  except:
    flash('db problem')

  form = DomainForm()

  if request.method == 'POST':
    if form.validate_on_submit():
      new_domain = Domain(
        0,
        domain=form.domain.data,
        domain_description=form.description.data)
      try:
        get_db().add_domain(new_domain)
        flash(f'{form.domain.data} was successfully added')
        domains = get_db().get_domains()
      except:
        flash('Something went wrong adding the domain')
    else:    
      flash('The form is invalid')

  return render_template('domain_list.html',domains=domains,form=form)

@bp.route('/<int:id>/')
def get_domain(id):
  try:
    domain = get_db().get_domain_by_id(id)
    return render_template('domain.html',domain=domain)
  except TypeError as e:
    flash(str(e))
  except:
    flash('Domain Not Found')

  return redirect(url_for('domains.get_domains'))

@bp.route('/<id>/modify',methods=['GET','POST'])
@login_required
def modify_domain(id):
  form = DomainForm()
  if request.method == 'POST':
    if form.validate_on_submit():
      chosen_domain = get_db().get_domain_by_id(id)
      if not chosen_domain == None:
        new_domain = Domain(
          domain_id=id,
          domain=form.domain.data,
          domain_description=form.description.data
        )
        try:
          get_db().modify_domain(new_domain)
          flash(f'{form.domain.data} was successfully edited')
          return redirect(url_for('domains.get_domain', id=id))
        except Exception as e:
          flash(str(e))
          return redirect(url_for('domains.get_domain', id=id))
      else:
        flash('domain does not exist')
        return redirect(url_for('domains.get_domain', id=id))
    else:
      flash('Form is Invalid') 
      return redirect(url_for('domains.get_domain', id=id))
  elif request.method == 'GET':
      domain = get_db().get_domain_by_id(id)
      return render_template('edit_domain.html',domain=domain,form=form)

@bp.route('/<id>/delete')
@login_required
def delete_domain(id):
  try:
    get_db().delete_domain(id)
    flash(f'Deleted domain {id}')
  except:
    flash(f'Could not delete Domain {id}')
  return redirect(url_for('domains.get_domains'))