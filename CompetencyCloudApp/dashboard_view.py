import os
from flask import Blueprint, abort, current_app, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required
import oracledb
from werkzeug.security import check_password_hash, generate_password_hash
from CompetencyCloudApp import user
from CompetencyCloudApp.group import add_user_to_group

from CompetencyCloudApp.user import MoveUserForm, RemoveUserForm, BlockMemberForm, UnblockMemberForm, SignupForm, User

from .dbmanager import get_db

bp = Blueprint('dashboard',__name__,url_prefix='/dashboard/')

@bp.route('/members/')
@login_required
def member_dashboard():
    members = get_db().get_all_user_profiles('member')
    return  render_template('dashboard_member.html', members=members)

@bp.route('/admin-user/')
@login_required
def admin_user_dashboard():
    members = get_db().get_all_user_profiles('member')
    if not (current_user.group_name == 'admin_user' or current_user.group_name == 'admin'):
        return abort(403)
    return  render_template('dashboard_admin_user.html', members=members)

@bp.route('/admin/')
@login_required
def admin_dashboard():
    members = get_db().get_all_user_profiles('member')
    admin_users = get_db().get_all_user_profiles('admin_user')
    admins = get_db().get_all_user_profiles('admin')
    if not current_user.group_name == 'admin':
        return abort(403)
    return  render_template('dashboard_admin.html', members=members, admin_users=admin_users, admins=admins)


@bp.route('/add-member/', methods=['GET', 'POST'])
@login_required
def add_member():
    return add_user('member')

@bp.route('/add-admin-user/', methods=['GET', 'POST'])
@login_required
def add_admin_user():
    return add_user('admin_user')

@bp.route('/add-admin/', methods=['GET', 'POST'])
@login_required
def add_admin():
    return add_user('admin')


def add_user(group_name):
    form = SignupForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            file = form.avatar.data
            avatar_dir = os.path.join(current_app.config['IMAGE_PATH'], form.email.data)
            if not os.path.exists(avatar_dir):
                os.makedirs(avatar_dir)
            avatar_path = os.path.join(avatar_dir, 'avatar.png')
            file.save(avatar_path)

            if get_db().get_user_by_email(form.email.data):
                flash("User already exists")
            else:
                hash = generate_password_hash(form.password.data)
                user = User(form.email.data, hash, form.name.data)
                try:
                    get_db().add_user(user, group_name)
                    add_user_to_group(user, group_name)
                    flash('User successfully signed up')
                except oracledb.Error:
                    flash('Could not add the user')
        else:
            flash('Form is Invalid')
    return render_template('add_user.html', form=form)



@bp.route('/remove-member/', methods=['GET', 'POST'])
@login_required
def remove_member():
    return remove_user('member')
    
@bp.route('/remove-admin-user/', methods=['GET', 'POST'])
@login_required
def remove_admin_user():
    return remove_user('admin_user')
    
@bp.route('/remove-admin/', methods=['GET', 'POST'])
@login_required
def remove_admin():
    return remove_user('admin')

def remove_user(group_name):
    form = RemoveUserForm()
    form.users.choices = [profile.profile_email for profile in get_db().get_all_user_profiles(group_name) if profile.profile_email != current_user.email]

    dashboard_url = f'dashboard.{group_name}_dashboard'

    if (len(form.users.choices) == 0):
        message = f'No {group_name}s to remove'
        if current_user.group_name == group_name:
            message += ' (except yourself)'
        flash(message)
        return redirect(url_for(dashboard_url))

    if request.method == 'POST' and form.validate_on_submit():
        chosen_user = get_db().get_user_by_email(form.users.data)
        if not chosen_user == None:
            get_db().delete_user(form.users.data)
            flash(f'{form.users.data} was successfully removed from database')
            members = get_db().get_all_user_profiles('member')
            admin_users = get_db().get_all_user_profiles('admin_user')
            admins = get_db().get_all_user_profiles('admin')
            dashboard = f'dashboard_{current_user.group_name}.html'
            return  render_template(dashboard, members=members, admin_users=admin_users, admins=admins)
        else:
            flash('user does not exist')
    elif request.method == 'GET':
        return render_template('remove_user.html', form=form)
    
    
@bp.route('/move-member/', methods=['GET', 'POST'])
@login_required
def move_member():
    return move_user('member')

@bp.route('/move-admin-user/', methods=['GET', 'POST'])
@login_required
def move_admin_user():
    return move_user('admin_user')

@bp.route('/move-admin/', methods=['GET', 'POST'])
@login_required
def move_admin():
    return move_user('admin')

def move_user(group_name):
    all_groups = ['member', 'admin_user', 'admin']
    user_group_name = current_user.group_name
    group_index = all_groups.index(user_group_name) + 1
    groups = all_groups[0:group_index]

    form = MoveUserForm()
    
    form.users.choices = [profile.profile_email for profile in get_db().get_all_user_profiles(group_name) if profile.profile_email != current_user.email]
    
    dashboard_url = f'dashboard.{group_name}_dashboard'

    if (len(form.users.choices) == 0):
        message = f'No {group_name}s to move'
        if current_user.group_name == group_name:
            message += ' (except yourself)'
        flash(message)
        return redirect(url_for(dashboard_url))
    
    form.groups.choices = groups
	
    if request.method == 'POST' and form.validate_on_submit():
        chosen_user = get_db().get_user_by_email(form.users.data)
        if not chosen_user == None:
            get_db().move_user_to_group(form.users.data, form.groups.data)
            flash(f'User was moved to {form.groups.data} Successfully')
            members = get_db().get_all_user_profiles('member')
            admin_users = get_db().get_all_user_profiles('admin_user')
            admins = get_db().get_all_user_profiles('admin')
            dashboard = f'dashboard_{current_user.group_name}.html'
            return  render_template(dashboard, members=members, admin_users=admin_users, admins=admins)
        else:
            flash('user does not exist')
    elif request.method == 'GET':
        return render_template('move_user.html', form=form)
    

@bp.route('/block-member/', methods=['GET', 'POST'])
@login_required
def block_member():
    if get_db().get_all_user_profiles('member') == None:
        flash('No Members to block')
        return redirect(url_for('dashboard.admin_dashboard'))
    form = BlockMemberForm()
    form.unblocked_members.choices = [member.profile_email for member in get_db().get_all_user_profiles('member')]
    if request.method == 'POST' and form.validate_on_submit():
        chosen_member = get_db().get_user_by_email(form.unblocked_members.data)
        if not chosen_member == None:
            get_db().block_user(form.unblocked_members.data)
            flash(f'{form.unblocked_members.data} was successfully blocked')
            members = get_db().get_all_user_profiles('member')
            admin_users = get_db().get_all_user_profiles('admin_user')
            admins = get_db().get_all_user_profiles('admin')
            return  render_template('dashboard_admin.html', members=members, admin_users=admin_users, admins=admins)
        else:
            flash('user does not exist')
            return redirect(url_for('dashboard.admin_dashboard'))
    elif request.method == 'GET':
        return render_template('block_member.html', form=form)
    
@bp.route('/unblock-member/', methods=['GET', 'POST'])
@login_required
def unblock_member():
    if get_db().get_all_user_profiles('member') == None:
        flash('No Members to unblock')
        return redirect(url_for('dashboard.admin_dashboard'))
    form = UnblockMemberForm()
    form.blocked_members.choices = [member.profile_email for member in get_db().get_all_blocked_user()]
    if request.method == 'POST' and form.validate_on_submit():
        chosen_member = get_db().get_user_by_email(form.blocked_members.data)
        if not chosen_member == None:
            get_db().unblock_user(form.blocked_members.data)
            flash(f'{form.blocked_members.data} was successfully unblocked')
            members = get_db().get_all_user_profiles('member')
            admin_users = get_db().get_all_user_profiles('admin_user')
            admins = get_db().get_all_user_profiles('admin')
            return  render_template('dashboard_admin.html', members=members, admin_users=admin_users, admins=admins)
        else:
            flash('user does not exist')
            return redirect(url_for('dashboard.admin_dashboard'))
    elif request.method == 'GET':
        return render_template('unblock_member.html', form=form)

def unblock_member():
    if get_db().get_all_user_profiles('admin') == None:
        flash('No Admins to move')
        return redirect(url_for('dashboard.admin_dashboard'))