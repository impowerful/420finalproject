from flask import (Blueprint, flash, redirect, render_template, request, url_for)

from CompetencyCloudApp.dbmanager import get_db
from CompetencyCloudApp.user import SearchForm
bp = Blueprint('home', __name__, url_prefix='/')

@bp.route("/")
def index():
    return render_template('index.html')

@bp.route('/search/<string:searched_query>/')
def searching_db(searched_query):
    try:
        all_course_results, all_competency_results, all_element_results, all_domain_results = get_db().search_whole_db(searched_query)
    except Exception as e:
        flash(str(e))
    return render_template('searched_result.html', all_course_results=all_course_results,all_competency_results=all_competency_results,all_element_results=all_element_results,all_domain_results=all_domain_results,searched_query=searched_query)

@bp.route('/searchbar',methods=['GET','POST'])
def searchbar():
    form = SearchForm()
    
    if request.method == 'GET':
        return render_template('searchbar.html', form=form)
    elif request.method == 'POST':
        if form.validate_on_submit():
            if form.search_query.data:
                return redirect(url_for('home.searching_db', searched_query=form.search_query.data))
    return render_template('searchbar.html', form=form)