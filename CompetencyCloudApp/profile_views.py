from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required
from werkzeug.security import check_password_hash, generate_password_hash
from CompetencyCloudApp import user

from CompetencyCloudApp.user import ResetPasswordForm

from .dbmanager import get_db

bp = Blueprint('profile',__name__,url_prefix='/profile/')

#  REMOVE THE PROFILE OBJECT

@bp.route('/<email>/')
@login_required
def get_profile(email):
    if not isinstance(email, str):
        raise TypeError ("Email has to be a string")
    user = get_db().get_user_by_email(email)
    if user is None:
        flash("User doesn't exist")
        return redirect(url_for('home.index'))
    return render_template('profile.html', user=user)

@bp.route('/reset-password/', methods=['GET', 'POST'])
@login_required
def reset_password():
    form = ResetPasswordForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            old_password = form.old_password.data
            if check_password_hash(current_user.password, old_password):
                new_password = form.new_password.data
                confirm_new_password = form.confirm_new_password.data
                if new_password == confirm_new_password:
                    if new_password == old_password:
                        flash('Reseting password requires new password to be different from the old one')
                        return redirect(url_for('profile.reset_password'))
                    hashed_new_password = generate_password_hash(new_password)
                    get_db().update_user_password(current_user.email, hashed_new_password)
                    flash('Password has be successfully changed')
                    return render_template('profile.html', user=current_user)
                else:
                    flash('The new passwords must match', category='invalid')
                    return redirect(url_for('profile.reset_password'))
            else:
                flash("The password entered doesn't match the old one")
                return redirect(url_for('profile.reset_password'))
        else:
            flash('Form is Invalid')
    elif request.method == 'GET':
        return render_template('reset_password.html', form=form)