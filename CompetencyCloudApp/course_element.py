class CourseElement:
    def __init__(self, course_id, element_id, element_hours):
        if not isinstance(course_id, str):
            raise TypeError('course id is not a str')
        self.course_id=course_id
        if isinstance(element_id,str) or isinstance(element_id,int):
            self.element_id=element_id
        else:
            raise TypeError('element id is wrong')
        if isinstance(element_hours,int) or isinstance(element_hours,float):
            self.element_hours=element_hours
        else:
            raise TypeError('element hour is not a number')
    
    def __repr__(self):
        return f'Element({self.course_id}, {self.element_id}, {self.element_hours})'
    
    def from_json(course_element_dict):
        pass    
    
from flask_wtf import FlaskForm
from wtforms import SelectField, IntegerField, StringField
from wtforms.validators import DataRequired  

class CourseElementForm(FlaskForm):
    element_id = SelectField('Element: ', validators=[DataRequired()])
    element_hours = IntegerField('Hours the element has: ', validators=[DataRequired()])
    
class DeleteCourseElementForm(FlaskForm):
    element_id = SelectField('Element that you want to delete: ', validators=[DataRequired()])