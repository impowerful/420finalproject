from flask import Blueprint, jsonify, request, url_for

from CompetencyCloudApp.dbmanager import get_db
from CompetencyCloudApp.term import Term

bp = Blueprint('term_api',__name__,url_prefix='/terms')

@bp.route('')
def all_terms():
  if request.method == 'GET':
    PAGE_NUM = 1
    PAGE_SIZE = 3

    page = request.args.get('page')
    if page:
      try: 
        PAGE_NUM = int(page)
      except:
        return {"description":"Page is not a number"}, 400
      
    try:
      terms,prev_page,next_page = get_db().get_terms(page_num=PAGE_NUM,page_size=PAGE_SIZE)
    except:
      return {"description":"Database Problem"}, 500
    
    id = request.args.get('id')
    if id:
      try:
        id = int(id)
      except:
        return {"description":"id is not an int"}, 400
      try:
        term = get_db().get_term(id)
      except:
        return {"description":"Database Problem"}, 500
      if term is None:
        return {"description":f"Term {id} does not exist"}, 404
      if term:
        return jsonify(term.to_json()),200
      
    prev_page_url = None
    next_page_url = None

    if prev_page:
      prev_page_url = url_for('api.term_api.all_terms',page=prev_page)
    if next_page:
      next_page_url = url_for('api.term_api.all_terms',page=next_page)
    
    json_page_terms = {'prev_page':prev_page_url,
                          'next_page':next_page_url,
                          'results':[term.to_json() for term in terms]}
    return jsonify(json_page_terms),200
  
  if request.method == 'POST':
    try:
      term = Term.from_json(request.json)
    except:
      return {"description":"Invalid term format"}, 400
    
    if not term:
      return {"description":"No term was provided"}, 400
    
    try:
      term = get_db().add_term(term)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Term added successfully"}, 201

  if request.method == 'PUT':
    try:
      term = Term.from_json(request.json)
    except:
      return {"description":"Invalid term format"}, 400
    
    if not term:
      return {"description":"No term was provided"}, 400
    
    try:
      term_to_update = get_db().get_term(term.term_id)
      if not term_to_update:
        return {"description":f"Term with id {term.term_id} does not exist"}, 404
      get_db().update_term(term)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Term updated successfully"}, 201
  
  if request.method == 'DELETE':
    id = request.args.get('id')
    if not id:
      return {"description":"No term id was provided"}, 400

    try:
      term_to_delete = get_db().get_term(id)
      if not term_to_delete:
        return {"description":f"Term with id {id} does not exist"}, 404
    except:
      return {"description":"Database Problem"}, 500
    
    try:
      get_db().delete_term(id)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Term deleted successfully"}, 201
  
  return {"description":"no GET,POST,PUT,DELETE request was made"},405
