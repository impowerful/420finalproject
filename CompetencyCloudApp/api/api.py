from flask import Blueprint
from .course_api import bp as course_api_bp
from .domain_api import bp as domain_api_bp
from .term_api import bp as term_api_bp
from .competency_api import bp as competency_api_bp
from .element_api import bp as element_api_bp

version = 'v1'

bp = Blueprint('api',__name__,url_prefix=f'/api/{version}/')

bp.register_blueprint(course_api_bp)
bp.register_blueprint(domain_api_bp)
bp.register_blueprint(term_api_bp)
bp.register_blueprint(competency_api_bp)
bp.register_blueprint(element_api_bp)

@bp.route('/')
def api_home():
  return f"CompetencyCloud REST API {version}"