from flask import Blueprint, jsonify, request, url_for

from CompetencyCloudApp.dbmanager import get_db
from CompetencyCloudApp.element import Element

bp = Blueprint('element_api',__name__,url_prefix='/elements')

@bp.route('',methods=['GET','POST'])
def all_elements():
  if request.method == 'GET':
    PAGE_NUM = 1
    PAGE_SIZE = 10
    page = request.args.get('page')
    if page:
      try: 
        PAGE_NUM = int(page)
      except:
        return {"description":"Page is not a number"}, 400
      
    try:
      elements,prev_page,next_page = get_db().get_elements(page_num=PAGE_NUM,page_size=PAGE_SIZE)
    except:
      return {"description":"Database Problem"}, 500
    
    id = request.args.get('id')
    if id:
      try:
        id = int(id)
      except:
        return {"description":"id is not an int"}, 400
      try:
        element = get_db().get_element(id)
      except:
        return {"description":"Database Problem"}, 500
      if element is None:
        return {"description":f"Element {id} does not exist"}, 404
      if element:
        return jsonify(element.to_json()),200
      
    prev_page_url = None
    next_page_url = None

    if prev_page:
      prev_page_url = url_for('api.element_api.all_elements',page=prev_page)
    if next_page:
      next_page_url = url_for('api.element_api.all_elements',page=next_page)
    
    json_page_elements = {'prev_page':prev_page_url,
                          'next_page':next_page_url,
                          'results':[element.to_json() for element in elements]}
    return jsonify(json_page_elements),200
  
  if request.method == 'POST':
    try:
      element = Element.from_json(request.json)
    except:
      return {"description":"Invalid element format"}, 400
    
    if not element:
      return {"description":"No element was provided"}, 400
    
    try:
      element = get_db().add_element(element)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Element added successfully"}, 201

  if request.method == 'PUT':
    try:
      element = Element.from_json(request.json)
    except:
      return {"description":"Invalid element format"}, 400
    
    if not element:
      return {"description":"No element was provided"}, 400
    
    try:
      element_to_update = get_db().get_element(element.element_id)
      if not element_to_update:
        return {"description":f"Element with id {element.element_id} does not exist"}, 404
      get_db().update_element(element)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Element updated successfully"}, 201
  
  if request.method == 'DELETE':
    id = request.args.get('id')
    if not id:
      return {"description":"No element id was provided"}, 400

    try:
      element_to_delete = get_db().get_element(id)
      if not element_to_delete:
        return {"description":f"Element with id {id} does not exist"}, 404
    except:
      return {"description":"Database Problem"}, 500
    
    try:
      get_db().delete_element(id)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Element deleted successfully"}, 201
  
  return {"description":"no GET,POST,PUT,DELETE request was made"},405

