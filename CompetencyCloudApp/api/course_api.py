from flask import Blueprint, jsonify, request, url_for
from CompetencyCloudApp.course import Course

from CompetencyCloudApp.dbmanager import get_db
from CompetencyCloudApp.course import Course

bp = Blueprint('course_api',__name__,url_prefix='/courses')

@bp.route('',methods=['GET', 'POST', 'PUT', 'DELETE'])
def all_courses():
  if request.method == 'GET':
    PAGE_NUM = 1
    PAGE_SIZE = 2
    page = request.args.get('page')
    if page:
      try: 
        PAGE_NUM = int(page)
      except:
        return {"description":"Page is not a number"}, 400
      
    try:
      courses,prev_page,next_page = get_db().get_courses(page_num=PAGE_NUM,page_size=PAGE_SIZE)
    except:
      return {"description":"Database Problem"}, 500
    
    id = request.args.get('id')
    if id:
      try:
        course = get_db().get_course(id)
      except:
        return {"description":"Database Problem"}, 500
      if course is None:
        return {"description":f"Course {id} does not exist"}, 404
      if course:
        return jsonify(course.to_json()),200
      
    prev_page_url = None
    next_page_url = None

    if prev_page:
      prev_page_url = url_for('api.course_api.all_courses',page=prev_page)
    if next_page:
      next_page_url = url_for('api.course_api.all_courses',page=next_page)
    
    json_page_courses = {'prev_page':prev_page_url,
                          'next_page':next_page_url,
                          'results':[course.to_json() for course in courses]}
    return jsonify(json_page_courses),200
  
  if request.method == 'POST':
    try:
      course = Course.from_json(request.json)
    except:
      return {"description":"Invalid course format"}, 400
    
    if not course:
      return {"description":"No course was provided"}, 400
    
    try:
      course = get_db().add_course(course)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Course added successfully"}, 201
  
  if request.method == 'PUT':
    try:
      course = Course.from_json(request.json)
    except:
      return {"description":"Invalid course format"}, 400
    
    if not course:
      return {"description":"No course was provided"}, 400
    
    try:
      course_to_update = get_db().get_course(id)
      if not course_to_update:
        return {"description":f"Course with id {id} does not exist"}, 404
      get_db().update_course(course)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Course updated successfully"}, 201

  if request.method == 'DELETE':
    id = request.args.get('id')
    if not id:
      return {"description":"No course id was provided"}, 400

    try:
      course_to_delete = get_db().get_course(id)
      if not course_to_delete:
        return {"description":f"Course with id {id} does not exist"}, 404
    except:
      return {"description":"Database Problem"}, 500
    
    try:
      get_db().delete_course(id)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Course deleted successfully"}, 201

  return {"description":"no GET,POST,PUT,DELETE request was made"},405