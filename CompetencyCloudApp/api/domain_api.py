from flask import Blueprint, jsonify, request, url_for

from CompetencyCloudApp.dbmanager import get_db
from CompetencyCloudApp.domain import Domain

bp = Blueprint('domain_api',__name__,url_prefix='/domains')

@bp.route('')
def all_domains():
  if request.method == 'GET':
    PAGE_NUM = 1
    PAGE_SIZE = 1

    page = request.args.get('page')
    if page:
      try: 
        PAGE_NUM = int(page)
      except:
        return {"description":"Page is not a number"}, 400
      
    try:
      domains,prev_page,next_page = get_db().get_domains(page_num=PAGE_NUM,page_size=PAGE_SIZE)
    except:
      return {"description":"Database Problem"}, 500
    
    id = request.args.get('id')
    if id:
      try:
        id = int(id)
      except:
        return {"description":"id is not an int"}, 400
      try:
        domain = get_db().get_domain_by_id(id)
      except:
        return {"description":"Database Problem"}, 500
      if domain is None:
        return {"description":f"Domain {id} does not exist"}, 404
      if domain:
        return jsonify(domain.to_json()),200
      
    prev_page_url = None
    next_page_url = None

    if prev_page:
      prev_page_url = url_for('api.domain_api.all_domains',page=prev_page)
    if next_page:
      next_page_url = url_for('api.domain_api.all_domains',page=next_page)
    
    json_page_domains = {'prev_page':prev_page_url,
                          'next_page':next_page_url,
                          'results':[domain.to_json() for domain in domains]}
    return jsonify(json_page_domains),200
  
  if request.method == 'POST':
    try:
      domain = Domain.from_json(request.json)
    except:
      return {"description":"Invalid domain format"}, 400
    
    if not domain:
      return {"description":"No domain was provided"}, 400
    
    try:
      domain = get_db().add_domain(domain)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Domain added successfully"}, 201
  
  if request.method == 'PUT':
    try:
      domain = Domain.from_json(request.json)
    except:
      return {"description":"Invalid domain format"}, 400
    
    if not domain:
      return {"description":"No domain was provided"}, 400
    
    try:
      domain_to_update = get_db().get_domain_by_id(domain.domain_id)
      if not domain_to_update:
        return {"description":f"Domain with id {domain.domain_id} does not exist"}, 404
      get_db().update_domain(domain)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Domain updated successfully"}, 201

  if request.method == 'DELETE':
    id = request.args.get('id')
    if not id:
      return {"description":"No domain id was provided"}, 400

    try:
      domain_to_delete = get_db().get_domain_by_id(id)
      if not domain_to_delete:
        return {"description":f"Domain with id {id} does not exist"}, 404
    except:
      return {"description":"Database Problem"}, 500
    
    try:
      get_db().delete_domain(id)
      pass
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Domain deleted successfully"}, 201
  
  return {"description":"no GET,POST,PUT,DELETE request was made"},405
