from flask import Blueprint, jsonify, request, url_for
from CompetencyCloudApp.competency import Competency

from CompetencyCloudApp.dbmanager import get_db

bp = Blueprint('competency_api',__name__,url_prefix='/competencies')

@bp.route('')
def all_competency():
  if request.method == 'GET':
    PAGE_NUM = 1
    PAGE_SIZE = 10

    page = request.args.get('page')
    if page:
      try: 
        PAGE_NUM = int(page)
      except:
        return {"description":"Page is not a number"}, 400
      
    try:
      competencies,prev_page,next_page = get_db().get_competencies(page_num=PAGE_NUM,page_size=PAGE_SIZE)
    except:
      return {"description":"Database Problem"}, 500
    
    id = request.args.get('id')
    if id:
      try:
        competency = get_db().get_competency(id)
      except:
        return {"description":"Database Problem"}, 500
      if competency is None:
        return {"description":f"Competency {id} does not exist"}, 404
      if competency:
        return jsonify(competency.to_json()),200
      
    prev_page_url = None
    next_page_url = None

    if prev_page:
      prev_page_url = url_for('api.competency_api.all_competency',page=prev_page)
    if next_page:
      next_page_url = url_for('api.competency_api.all_competency',page=next_page)
    
    json_page_all_competencies = {'prev_page':prev_page_url,
                          'next_page':next_page_url,
                          'results':[competency.to_json() for competency in competencies]}
    return jsonify(json_page_all_competencies),200
  
  if request.method == 'POST':
    try:
      competency = Competency.from_json(request.json)
    except:
      return {"description":"Invalid competency format"}, 400
    
    if not competency:
      return {"description":"No competency was provided"}, 400
    
    try:
      competency = get_db().add_competency(competency)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Competency added successfully"}, 201
  
  if request.method == 'PUT':
    try:
      competency = Competency.from_json(request.json)
    except:
      return {"description":"Invalid competency format"}, 400
    
    if not competency:
      return {"description":"No competency was provided"}, 400
    
    try:
      competency_to_update = get_db().get_competency(competency.competency_id)
      if not competency_to_update:
        return {"description":f"Competency with id {competency.competency_id} does not exist"}, 404
      get_db().update_competency(competency)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Competency updated successfully"}, 201
  
  if request.method == 'DELETE':
    id = request.args.get('id')
    if not id:
      return {"description":"No competency id was provided"}, 400

    try:
      competency_to_delete = get_db().get_competency(id)
      if not competency_to_delete:
        return {"description":f"Competency with id {id} does not exist"}, 404
    except:
      return {"description":"Database Problem"}, 500
    
    try:
      get_db().delete_competency(id)
    except:
      return {"description":"Database Problem"}, 500
    
    return {"description": "Competency deleted successfully"}, 201
  
  return {"description":"no GET,POST,PUT,DELETE request was made"},405