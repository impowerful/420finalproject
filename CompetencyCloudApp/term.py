from flask import url_for


class Term:
  def __init__(self,term_id,term_name):
    if not isinstance(term_id,int):
      raise TypeError('term_id is not an int')
    if not isinstance(term_name,str):
      raise TypeError('name is not a string')
    
    self.term_id = term_id
    self.term_name = term_name

  def __repr__(self):
    return f'Term({self.term_id}, {self.term_name})'

  def from_json(course_dict):
    return Term(course_dict['term_id'],
                course_dict['term_name'])

  def to_json(self):
    termUrl = url_for('api.term_api.all_terms',id=self.term_id)
    return {"url":termUrl,
            "term_id":self.term_id,
            "term_name":self.term_name}