from flask import url_for
from CompetencyCloudApp.competency import Competency

class Element:
  def __init__(self,element_id,element_order,element_name,element_criteria,competency):
    if not isinstance(element_id,int):
      raise TypeError('element_id is not an int')
    if not isinstance(element_order,int):
      raise TypeError('element_order is not an int')
    if not isinstance(element_name,str):
      raise TypeError('element_name is not a str')
    if not isinstance(element_criteria,str):
      raise TypeError('element_criteria is not a str')
    if not isinstance(competency,Competency):
      raise TypeError('competency is not a Competency')
    
    self.element_id = element_id
    self.element_order = element_order
    self.element_name = element_name
    self.element_criteria = element_criteria
    self.competency = competency

  def __repr__(self):
    return f'Element({self.element_id}, {self.element_order}, {self.element_name}, {self.element_criteria}, {self.competency})'
  
  def to_json(self):
    elementUrl = url_for('api.element_api.all_elements',id=self.element_id)
    return {'url':elementUrl,
            'element_id':self.element_id,
            'element_order':self.element_order,
            'element_name':self.element_name,
            'element_criteria':self.element_criteria,
            'competency':self.competency.to_json()}