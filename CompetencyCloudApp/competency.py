from flask import url_for


class Competency:
  def __init__(self, competency_id, competency_title, competency_achievement, competency_type):
    if not isinstance(competency_id, str):
      raise TypeError('competency_id is not a string')
    if not isinstance(competency_title, str):
      raise TypeError('competency_title is not a string')
    if not isinstance(competency_achievement, str):
      raise TypeError('competency_achievement is not a string')
    if not isinstance(competency_type, str):
      raise TypeError('competency_type is not a string')

    self.competency_id = competency_id
    self.competency_title = competency_title
    self.competency_achievement = competency_achievement
    self.competency_type = competency_type
    
  def __repr__(self):
    return f'Competency({self.competency_id}, {self.competency_title}, {self.competency_achievement}, {self.competency_type})'
  
  def from_json(competency_dict):
    pass

  def to_json(self):
    competencyUrl = url_for('api.competency_api.all_competency',id=self.competency_id)
    return {"url":competencyUrl,
            "competency_id":self.competency_id,
            "competency_title":self.competency_title,
            "competency_achievement":self.competency_achievement,
            "competency_type":self.competency_type}
  
from flask_wtf import FlaskForm
from wtforms import StringField,IntegerField,TextAreaField
from wtforms.validators import DataRequired,Regexp
class CompetencyForm(FlaskForm):
  competency_id = StringField('Competency ID:',validators=[DataRequired(),Regexp('^[0]{2}[A-Z]{1}[A-Z0-9]{1}$')])
  competency_title = StringField('Competency Title:',validators=[DataRequired()])
  competency_achievement = StringField('Competency Achievement:',validators=[DataRequired()])
  competency_type = StringField('Competency Type:',validators=[DataRequired()])
  
class CompetencyFormModify(FlaskForm):
  competency_title = StringField('Competency Title:',validators=[DataRequired()])
  competency_achievement = StringField('Competency Achievement:',validators=[DataRequired()])
  competency_type = StringField('Competency Type:',validators=[DataRequired()])