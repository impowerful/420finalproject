import os
from flask import Blueprint, abort, current_app, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required
import oracledb
from CompetencyCloudApp import course_element

from CompetencyCloudApp.course_element import CourseElementForm, DeleteCourseElementForm, CourseElement
from .dbmanager import get_db

bp = Blueprint('course_element',__name__,url_prefix='/element/')

@bp.route('/add-course-element/<string:course_id>/',methods=['GET','POST'])
@login_required
def add_course_element(course_id):
    form = CourseElementForm()
    form.element_id.choices = get_db().get_all_elements()
    if request.method == 'POST' and form.validate_on_submit():
        chosen_element = get_db().get_course_elements_by_element_id(form.element_id.data)
        if not chosen_element == None:
            get_db().add_course_elements(course_id, form.element_id.data, form.element_hours.data)
            flash(f'{form.element_id.data} was successfully added to {course_id}')
            return redirect(url_for('courses.get_course', id=course_id))
        else:
            flash('element does not exist')
    elif request.method == 'GET':
        return render_template('add_course_element.html', form=form)
        
@bp.route('/edit-course-element/<string:course_id>/',methods=['GET','POST'])
@login_required
def edit_course_element(course_id):
    form = CourseElementForm()
    form.element_id.choices = [elements.element_id for elements in get_db().get_elements_course_id(course_id)]
    if request.method == 'POST' and form.validate_on_submit():
        chosen_element = get_db().get_course_elements_by_element_id(form.element_id.data)
        if not chosen_element == None:
            get_db().edit_course_elements(course_id, form.element_id.data, form.element_hours.data)
            flash(f'{form.element_id.data} was successfully edited')
            return redirect(url_for('courses.get_course', id=course_id))
        else:
            flash('element does not exist')
    elif request.method == 'GET':
        return render_template('edit_course_element.html', form=form)
    
@bp.route('/delete-course-element/<string:course_id>/',methods=['GET','POST'])
@login_required
def delete_course_element(course_id):
    form = DeleteCourseElementForm()
    form.element_id.choices = [elements.element_id for elements in get_db().get_elements_course_id(course_id)]
    if request.method == 'POST' and form.validate_on_submit():
        chosen_element = get_db().get_course_elements_by_element_id(form.element_id.data)
        if not chosen_element == None:
            get_db().delete_course_elements(course_id, form.element_id.data)
            flash(f'{form.element_id.data} was successfully deleted from {course_id}')
            return redirect(url_for('courses.get_course', id=course_id))
        else:
            flash('element does not exist')
    elif request.method == 'GET':
        return render_template('delete_course_element.html', form=form) 