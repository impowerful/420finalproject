from flask_login import UserMixin

class User(UserMixin):
    def __init__(self, email, password, name, id=None, group_name='member', blocked_status=False):
        if not isinstance(email, str):
            raise TypeError()
        if not isinstance(password, str):
            raise TypeError()
        if not isinstance(name, str):
            raise TypeError()
        if id and not isinstance(id, int):
            raise TypeError()
        self.email = email
        self.password = password
        self.name = name
        self.id = id
        self.group_name = group_name
        self.blocked_status = blocked_status


from flask_wtf import FlaskForm
from wtforms import EmailField, PasswordField, StringField, BooleanField, FileField, SelectField
from wtforms.validators import DataRequired

class SignupForm(FlaskForm):
    email = EmailField('email:', validators=[DataRequired()])
    password = PasswordField('password:', validators=[DataRequired()])
    name = StringField('name:', validators=[DataRequired()])
    avatar = FileField('avatar:', validators=[DataRequired()])

class LoginForm(FlaskForm):
    email = EmailField('email:', validators=[DataRequired()])
    password = PasswordField('password:', validators=[DataRequired()])
    remember_me = BooleanField('remember me')
    
class ResetPasswordForm(FlaskForm):
    old_password = PasswordField('old password:', validators=[DataRequired()])
    new_password = PasswordField('new password:', validators=[DataRequired()])
    confirm_new_password = PasswordField('confirm new password:', validators=[DataRequired()])
    
class RemoveUserForm(FlaskForm):
    users = SelectField('Select the User to Delete')
    
class BlockMemberForm(FlaskForm):
    unblocked_members = SelectField('Select the Member to Block')
    
class UnblockMemberForm(FlaskForm):
    blocked_members = SelectField('Select the Member to Unblock')

class MoveUserForm(FlaskForm):
    users = SelectField('Select the User to Move', validators=[DataRequired()])
    groups = SelectField('Select the Group')
    
class SearchForm(FlaskForm):
    search_query = StringField('SearchBar: ', validators=[DataRequired()])