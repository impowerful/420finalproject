from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required

from .course import Course,CourseForm, CourseFormModify
from .term import Term
from .domain import Domain
from .dbmanager import get_db

bp = Blueprint('courses',__name__,url_prefix='/courses/')

@bp.route('/',methods=['GET','POST'])
def get_courses():
  try:
    courses, prev_page, next_page = get_db().get_courses()
  except:
    flash('db problem')

  form = CourseForm()

  if request.method == 'POST':
    if form.validate_on_submit():
      try:
        domain = get_db().get_domain_by_id(domain_id=form.domain_id.data)
        term = get_db().get_term(term_id=form.term_id.data)
      except:
        flash('Domain or Term do not exist')
        return render_template('course_list.html',courses=courses,form=form)
      new_course = Course(
        course_id=form.course_id.data,
        course_title=form.course_title.data,
        theory_hours=form.theory_hours.data,
        lab_hours=form.lab_hours.data,
        work_hours=form.work_hours.data,
        description=form.description.data,
        domain=domain,
        term=term)
      try:
        get_db().add_course(new_course)
        courses, prev_page, next_page = get_db().get_courses()
      except:
        flash("Something went wrong when adding the course")
  
  return render_template('course_list.html',courses=courses,form=form)

@bp.route('/<id>/',methods=['GET','POST'])
def get_course(id):
  try:
    course = get_db().get_course(id)
    elements = get_db().get_elements_course_id(id)
    return render_template('course.html',course=course,elements=elements)
  except TypeError as e:
    flash(str(e))
  except:
    flash('Course not found')

  return redirect(url_for('courses.get_courses'))

@bp.route('/<id>/modify',methods=['GET','POST'])
@login_required
def modify_course(id):
  form = CourseFormModify()
  if request.method == 'POST':
    if form.validate_on_submit():
      chosen_course = get_db().get_course(id)
      if not chosen_course == None:
        try:
          domain = get_db().get_domain_by_id(domain_id=form.domain_id.data)
          term = get_db().get_term(term_id=form.term_id.data)
        except:
          flash('Domain or Term do not exist') 
          return redirect(url_for('courses.get_course', id=id))
        new_course = Course(
          course_id=id,
          course_title=form.course_title.data,
          theory_hours=form.theory_hours.data,
          lab_hours=form.lab_hours.data,
          work_hours=form.work_hours.data,
          description=form.description.data,
          domain=domain,
          term=term)
        try:
          get_db().update_course(new_course)
          flash(f'{form.course_title.data} was successfully edited')
          return redirect(url_for('courses.get_course', id=id))
        except:
          flash("Something went wrong modifying the course")
      else:
        flash('course does not exist')
    else:
      flash('Form is Invalid')
    return redirect(url_for('courses.get_course', id=id))
  elif request.method == 'GET':
      course = get_db().get_course(id)
      return render_template('edit_course.html',course=course,form=form)

@bp.route('/<id>/delete')
@login_required
def delete_course(id):
  try:
    get_db().delete_course(id)
    flash(f'Deleted Course {id}')
  except:
    flash(f'Could not delete Course {id}')
  return redirect(url_for('courses.get_courses'))