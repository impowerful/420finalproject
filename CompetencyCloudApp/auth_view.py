from flask import Blueprint, current_app, flash, redirect, render_template, request, send_from_directory, url_for
from flask_login import login_user, logout_user, login_required
import oracledb
from CompetencyCloudApp.dbmanager import get_db
from werkzeug.security import generate_password_hash, check_password_hash
from CompetencyCloudApp.user import LoginForm, SignupForm, User
from CompetencyCloudApp.group import Privilege, requires_privilege, add_user_to_group, add_admin, add_admin_user, add_members
import os

bp = Blueprint('auth', __name__, url_prefix='/auth/')

@bp.route('/signup/', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            file = form.avatar.data
            avatar_dir = os.path.join(current_app.config['IMAGE_PATH'], form.email.data)
            if not os.path.exists(avatar_dir):
                os.makedirs(avatar_dir)
            avatar_path = os.path.join(avatar_dir, 'avatar.png')
            file.save(avatar_path)

            if get_db().get_user_by_email(form.email.data):
                flash("User already exists")
            else:
                hash = generate_password_hash(form.password.data)
                user = User(form.email.data, hash, form.name.data)
                try:
                    get_db().add_user(user)
                    add_user_to_group(user, 'member')
                    flash('User successfully signed up')
                except oracledb.Error:
                    flash('Could not add the user')
                
    return render_template('signup.html', form=form)


@bp.route('/login/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            email = form.email.data
            user = get_db().get_user_by_email(email)
            if user:
                pwd = form.password.data
                status = get_db().get_blocked_status_by_email(email)
                if status == '0':
                    if check_password_hash(user.password, pwd):
                        login_user(user, remember=form.remember_me.data)
                        flash("Successfully logged in")
                    else:
                        flash("Invalid credentials")
                else:
                    flash("Your account has been blocked please contact a Admin")
                    redirect(url_for('auth.login'))
            else:
                flash("Could not find user")
                redirect(url_for('auth.signup'))
        else:
            flash("Invalid form")
    return render_template('login.html', form=form)

@bp.route('/logout/')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))

@bp.route('/avatar/<email>/avatar.png')
def get_avatar(email):
    avatar_dir = os.path.join(current_app.config['IMAGE_PATH'], email)
    return send_from_directory(avatar_dir, 'avatar.png')

@bp.route('/add-users')
def add_users():
    add_admin()
    add_admin_user()
    add_members()
    return redirect(url_for('auth.login'))