from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required

from .competency import Competency,CompetencyForm, CompetencyFormModify
from .dbmanager import get_db

bp = Blueprint('competency',__name__,url_prefix='/competency/')

@bp.route('/',methods=['GET','POST'])
def get_competencies():
  try:
    competencies,prev_page,next_page = get_db().get_competencies()
  except:
    flash('db problem')

  form = CompetencyForm()
  
  if request.method == 'POST':
    if form.validate_on_submit():
      new_competency = Competency(
        competency_id=form.competency_id.data,
        competency_title=form.competency_title.data,
        competency_achievement=form.competency_achievement.data,
        competency_type=form.competency_type.data
      )
      try:
        get_db().add_competency(new_competency)
        competencies = get_db().get_competencies()
      except Exception as e:
        flash(str(e))
    else:
      flash('form invalid')
        
  return render_template('competency_list.html', competencies=competencies, form=form)

@bp.route('/<id>/')
def get_competency(id):
  try:
    competency = get_db().get_competency(id)
    elements = get_db().get_elements_competency_id(id)
    return render_template('competency.html',competency=competency,elements=elements)
  except TypeError as e:
    flash(str(e))
  except:
    flash('Competency not found')
  
    
  return redirect(url_for('competency.get_competencies'))

@bp.route('/<id>/modify',methods=['GET','POST'])
@login_required
def modify_competency(id):
  form = CompetencyFormModify()
  if request.method == 'POST':
    if form.validate_on_submit():
      chosen_competency = get_db().get_competency(id)
      if not chosen_competency == None:
        new_competency = Competency(
          competency_id=id,
          competency_title=form.competency_title.data,
          competency_achievement=form.competency_achievement.data,
          competency_type=form.competency_type.data
          )
        try:
          get_db().modify_competency(new_competency)
          flash(f'{form.competency_title.data} was successfully edited')
          return redirect(url_for('competency.get_competency', id=id))
        except:
          flash("Something went wrong modifying the competency")
          return redirect(url_for('competency.get_competency', id=id))
      else:
        flash('competency does not exist')
        return redirect(url_for('competency.get_competency', id=id))
    else:
      flash('Form is Invalid') 
      return redirect(url_for('competency.get_competency', id=id))
  elif request.method == 'GET':
      competency = get_db().get_competency(id)
      return render_template('edit_competency.html',competency=competency,form=form)
  
@bp.route('/<id>/delete')
@login_required
def delete_competency(id):
  try:
    get_db().delete_competency(id)
    flash(f'Deleted Competency {id}')
  except:
    flash(f'Could not delete Competency {id}')
  return redirect(url_for('competency.get_competencies'))