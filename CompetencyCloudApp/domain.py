from flask import url_for


class Domain:
  def __init__(self,domain_id,domain,domain_description):
    if isinstance(domain_id,int) or isinstance(domain_id,str):
      self.domain_id = domain_id
    else:
      raise TypeError('domain_id is not an int')
    if not isinstance(domain,str):
      raise TypeError('domain is not a string')
    if not isinstance(domain_description,str):
      raise TypeError('domain_description is not a string')
    
    
    self.domain = domain
    self.domain_description = domain_description

  def __repr__(self):
    return f'Domain({self.domain_id}, {self.domain}, {self.domain_description})'

  def from_json(course_dict):
    return Domain(course_dict['domain_id'],
                  course_dict['domain'],
                  course_dict['domain_description'])

  def to_json(self):
    domainUrl = url_for('api.domain_api.all_domains',id=self.domain_id)
    return {"url":domainUrl,
            "domain_id":self.domain_id,
            "domain":self.domain,
            "domain_description":self.domain_description}

from flask_wtf import FlaskForm
from wtforms import StringField,TextAreaField
from wtforms.validators import DataRequired

class DomainForm(FlaskForm):
  domain = StringField('Domain Name',validators=[DataRequired()])
  description = StringField('Description',validators=[DataRequired()])