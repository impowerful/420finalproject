class Profile:
    def __init__(self, profile_name, profile_email):
        if not isinstance(profile_name, str):
            raise TypeError('profile_name is not a string')
        if not isinstance(profile_email, str):
            raise TypeError('profile_email is not a string')
        
        self.profile_name = profile_name
        self.profile_email = profile_email
        
    def __repr__(self):
        return f'Profile Name: {self.profile_name}  Profile Email:  {self.profile_email}'
    
    def __str__(self):
        pass
    
    def from_json(profile_dict):
        pass