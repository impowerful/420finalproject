from functools import wraps
from flask import abort

from flask_login import current_user
from enum import Enum
from CompetencyCloudApp.dbmanager import get_db
from werkzeug.security import generate_password_hash
from CompetencyCloudApp.user import User

class Privilege(Enum):
    ADD_MEMBER = 1
    REMOVE_MEMBER = 2
    BLOCK_MEMBER = 3
    UNBLOCK_MEMBER = 4
    ADD_ADMIN_USER = 5
    REMOVE_ADMIN_USER = 6
    MOVE_USER_GROUP = 7
    ADD_ADMIN = 8
    REMOVE_ADMIN = 9
    SEE_OTHER_MEMBERS = 10
    SEE_OTHER_MEMBERS_GROUP = 11
    
class Group:
    member_privileges =     [Privilege.SEE_OTHER_MEMBERS]

    admin_user_privileges = [Privilege.ADD_MEMBER, 
                             Privilege.REMOVE_MEMBER, 
                             Privilege.BLOCK_MEMBER,
                             Privilege.UNBLOCK_MEMBER]
    
    admin_privileges =      [Privilege.ADD_ADMIN_USER, 
                             Privilege.REMOVE_ADMIN_USER, 
                             Privilege.MOVE_USER_GROUP, 
                             Privilege.ADD_ADMIN, 
                             Privilege.REMOVE_ADMIN, 
                             Privilege.SEE_OTHER_MEMBERS_GROUP]

    def __init__(self, name):
        self.name = name
        self.privileges = []
        self.users = []

    def set_privileges(self, privileges):
        for privilege in privileges:
            self.privileges.append(privilege)
    
    def remove_user(self, user):
        self.users.remove(user)

    def add_user(self, user):
        self.users.append(user)

class Member(Group):
    def __init__(self):
        super().__init__('member')
        super().set_privileges(Group.member_privileges)
        
class AdminUser(Group):
    def __init__(self):
        super().__init__('admin_user')
        super().set_privileges(Group.member_privileges)
        super().set_privileges(Group.admin_user_privileges)

class Admin(Group):
    def __init__(self):
        super().__init__('admin')
        super().set_privileges(Group.member_privileges)
        super().set_privileges(Group.admin_user_privileges)
        super().set_privileges(Group.admin_privileges)

member_group = Member()
admin_user_group = AdminUser()
admin_group = Admin()

# Decorator function to check if a user has the required privilege to perform action
# Can be called before method header like this:
# @requires_privilege(Privilege.ADD_MEMBER)
def requires_privilege(req_privilege):
    def decorator(func):
        @wraps(func)
        def decorated_function(*args, **kwargs):
            privileges = get_privileges_by_group_name(current_user.group_name)
            
            for privilege in privileges:
                if req_privilege == privilege:
                    return func(*args, **kwargs)
                
            abort(403)
        return decorated_function
    return decorator

def get_privileges_by_group_name(group_name):
    if group_name == 'member':
        return Member().privileges
    elif group_name == 'admin':
        return Admin().privileges
    elif group_name == 'admin_user':
        return AdminUser().privileges
    else:
        return None
    
def add_user_to_group(user, group_name):
    if group_name == 'member':
        member_group.add_user(user)
    elif group_name == 'admin':
        admin_group.add_user(user)
    elif group_name == 'admin_user':
        admin_user_group.add_user(user)
    
def add_members():
    pwd = 'member'
    hash = generate_password_hash(pwd)
    name = 'member'
    for i in range(1, 11):
        user = User('member' + str(i) + '@gmail.com', hash, name + str(i), group_name='member')
        get_db().add_user(user)
        member_group.add_user(user)

def add_admin_user():
    pwd = 'adminuser'
    hash = generate_password_hash(pwd)
    admin_user = User('adminuser@gmail.com', hash, 'Admin User', group_name='admin_user')
    get_db().add_user(admin_user, 'admin_user')
    admin_user_group.add_user(admin_user)

def add_admin():
    pwd = 'Python420'
    hash = generate_password_hash(pwd)
    admin = User('instructor@dawsoncollege.qc.ca', hash, 'Instructor', group_name='admin')
    get_db().add_user(admin, 'admin')
    admin_group.add_user(admin)