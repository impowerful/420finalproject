from flask import url_for
from .domain import Domain
from .term import Term
class Course:
  def __init__(self,course_id,course_title,theory_hours,lab_hours,work_hours,description,domain,term):
    if not isinstance(course_id,str):
      raise TypeError('course_id is not a string')
    if not isinstance(course_title,str):
      raise TypeError('course_title is not a string')
    if not isinstance(theory_hours,int):
      raise TypeError('theory_hours is not a int')
    if not isinstance(lab_hours,int):
      raise TypeError('lab_hours is not a int')
    if not isinstance(work_hours,int):
      raise TypeError('work_hours is not a int')
    if not isinstance(description,str):
      raise TypeError('description is not a string')
    if not isinstance(domain,Domain):
      raise TypeError('domain is not a Domain')
    if not isinstance(term,Term):
      raise TypeError('term is not a Term')
    
    self.course_id = course_id
    self.course_title = course_title
    self.theory_hours = theory_hours
    self.lab_hours = lab_hours
    self.work_hours = work_hours
    self.description = description
    self.domain = domain
    self.term = term

  def __repr__(self):
    return f'Course({self.course_id}, {self.course_title}, {self.theory_hours}, {self.lab_hours}, {self.work_hours}, {self.description}, {self.domain}, {self.term})'

  def from_json(course_dict):
    return Course(course_dict['course_id'],
                  course_dict['course_title'],
                  course_dict['theory_hours'],
                  course_dict['lab_hours'],
                  course_dict['work_hours'],
                  course_dict['description'],
                  Domain.from_json(course_dict['domain']),
                  Term.from_json(course_dict['term']))

  def to_json(self):
    courseUrl = url_for('api.course_api.all_courses',id=self.course_id)
    return {"url":courseUrl,
            "course_id":self.course_id,
            "course_title":self.course_title,
            "theory_hours":self.theory_hours,
            "lab_hours":self.lab_hours,
            "work_hours":self.work_hours,
            "description":self.description,
            "domain":self.domain.to_json(),
            "term":self.term.to_json()}
  
from flask_wtf import FlaskForm
from wtforms import StringField,IntegerField,TextAreaField
from wtforms.validators import DataRequired,Regexp
class CourseForm(FlaskForm):
  course_id = StringField('Course id: ',validators=[DataRequired()])
  course_title = StringField('Course Title: ',validators=[DataRequired()])
  theory_hours = IntegerField('Theory Hours: ',validators=[DataRequired()])
  lab_hours = IntegerField('Lab Hours: ',validators=[DataRequired()])
  work_hours = IntegerField('Work Hours: ',validators=[DataRequired()])
  description = StringField('Description: ',validators=[DataRequired()])
  domain_id = IntegerField('Domain id: ',validators=[DataRequired()])
  term_id = IntegerField('Term id: ',validators=[DataRequired()])

class CourseFormModify(FlaskForm):
  course_title = StringField('Course Title',validators=[DataRequired()])
  theory_hours = IntegerField('Theory Hours',validators=[DataRequired()])
  lab_hours = IntegerField('Lab Hours',validators=[DataRequired()])
  work_hours = IntegerField('Work Hours',validators=[DataRequired()])
  description = StringField('Description',validators=[DataRequired()])
  domain_id = IntegerField('Domain id',validators=[DataRequired()])
  term_id = IntegerField('Term id',validators=[DataRequired()])

