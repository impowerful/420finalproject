import os
from flask import Blueprint, abort, current_app, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required
import oracledb
from CompetencyCloudApp import competency_element

from CompetencyCloudApp.competency_element import AddCompetencyElementForm, DeleteCompetencyElementForm, EditCompetencyElementForm, CompetencyElement
from .dbmanager import get_db

bp = Blueprint('competency_element',__name__,url_prefix='/element/')

@bp.route('/add-competency-element/<string:competency_id>/',methods=['GET','POST'])
@login_required
def add_competency_element(competency_id):
    form = AddCompetencyElementForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            newElement = CompetencyElement(
                element_id= '1',
                element_name=form.element_name.data,
                element_order=form.element_order.data,
                element_criteria=form.element_criteria.data,
                competency_id=competency_id
            )
            try:
                get_db().add_competency_elements(newElement)
                flash('element was successfully added')
                return redirect(url_for('competency.get_competency', id=competency_id))
            except:
                flash('Something went wrong when adding the element')
                return redirect(url_for('competency.get_competency', id=competency_id))
        else:
            flash('Form is Invalid') 
            return redirect(url_for('competency.get_competency', id=competency_id))  
    elif request.method == 'GET':
        return render_template('add_competency_element.html', form=form)
    
@bp.route('/edit-competency-element/<string:competency_id>/',methods=['GET','POST'])
@login_required
def edit_competency_element(competency_id):
    form = EditCompetencyElementForm()
    form.element_id.choices = [elements.element_id for elements in get_db().get_elements_competency_id(competency_id)]
    if request.method == 'POST' and form.validate_on_submit():
        chosen_element = get_db().get_competency_elements_by_element_id(form.element_id.data)
        if not chosen_element == None:
            newElement = CompetencyElement(
                element_id= form.element_id.data,
                element_name=form.element_name.data,
                element_order=form.element_order.data,
                element_criteria=form.element_criteria.data,
                competency_id=competency_id
            )
            try:
                get_db().edit_competency_elements(newElement)
                flash(f'{form.element_id.data} was successfully edited')
                return redirect(url_for('competency.get_competency',id=competency_id))
            except:
                flash('Something went wrong when editing the element')
                return redirect(url_for('competency.get_competency',id=competency_id))
        else:
            flash('element does not exist')
            return redirect(url_for('competency.get_competency',id=competency_id))
    elif request.method == 'GET':
        return render_template('edit_competency_element.html', form=form)

@bp.route('/delete-competency-element/<string:competency_id>/',methods=['GET','POST'])
@login_required
def delete_competency_element(competency_id):
    form = DeleteCompetencyElementForm()
    form.element_id.choices = [elements.element_id for elements in get_db().get_elements_competency_id(competency_id)]
    if request.method == 'POST' and form.validate_on_submit():
        chosen_element = get_db().get_competency_elements_by_element_id(form.element_id.data)
        if not chosen_element == None:
            get_db().delete_competency_elements(competency_id, form.element_id.data)
            flash(f'{form.element_id.data} was successfully deleted from {competency_id}')
            return redirect(url_for('competency.get_competency', id=competency_id))
        else:
            flash('element does not exist')
            return redirect(url_for('competency.get_competency',id=competency_id))
    elif request.method == 'GET':
        return render_template('delete_competency_element.html', form=form) 